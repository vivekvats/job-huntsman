import "dotenv/config";

export default {
  expo: {
    scheme: "acme",
    web: {
      bundler: "metro",
    },
    plugins: ["expo-router"],
    name: "Job Huntsman",
    slug: "job-huntsman",
    extra: {
      rapidApiKey: process.env.RAPID_API_KEY,
    },
  },
};
